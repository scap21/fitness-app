# README #
Fitness App Group
Members: Drew Bae, Zaid Ali, Steven Capleton (scap21)

### Changelog ###
For each version just add your contributions.

v. 0.1 - Base
* setup view controllers
* made buttons for the main menu view controller
* made the initial food view controller
v. 0.1.01
* refined the functionality of the food view controller
* the totalCalories count now adds and subtracts properly
* the totalCalories count now saves if the view is left
* the totalCalories count will reload previous value when food view is reloaded