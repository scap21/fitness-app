//
//  FoodDetailViewController.swift
//  fitness
//
//  Created by Steven Capleton on 3/29/21.
//

import UIKit
import CoreData

class FoodDetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    // Outlet Identifiers
    @IBOutlet var foodTableView: UITableView!
    @IBOutlet var caloriesCount: UILabel!
    
    // Other Variables
    var parentVC: MainViewController!
    var editItem: Bool!
    var foods = [Food]()
    var selectedIndex: Int!
    var totalCalories: Double!
    
    // When view is loaded do initial setup
    override func viewDidLoad() {
        super.viewDidLoad()
        
        totalCalories = UserDefaults.standard.double(forKey: "CaloriesGained")
        caloriesCount.text = String(format: "%.2f", totalCalories)
        
        foodTableView.delegate = self
        foodTableView.dataSource = self
        
        // If returning to view controller retrieve foods from db
        getFoodFromDb()
    }
    
    // When view is closed save total calories to user defaults to load later
    override func viewDidDisappear(_ animated: Bool) {
        // If list did not save properly clear everything and start over
        if foods.count == 0 { UserDefaults.standard.set(0.0, forKey: "CaloriesGained") }
        // Else save total calories
        else { UserDefaults.standard.set(totalCalories, forKey: "CaloriesGained") }
    }
    
    // Add Button Pressed
    @IBAction func handleAddFood(_ sender: Any) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {return}
        let managedContext = appDelegate.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "Food", in: managedContext)!
        
        let foodItem = NSManagedObject(entity: entity, insertInto: managedContext) as! Food
        
        do {
            try managedContext.save()
            selectedIndex = foods.count
            editItem = false
            createAlert()
            foods.append(foodItem)
//            try managedContext.save()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    // Get the table item count
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return foods.count
    }
    
    // Populate table data
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FoodCell", for: indexPath) as! FoodTableCell
        cell.selectionStyle = .none
        cell.foodName.text = foods[indexPath.row].name
        cell.calCount.text = foods[indexPath.row].calories.description
        return cell
    }
    
    // Perform action at selected cell
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        editItem = true
        createAlert()
    }
    
    // Retrieve food items from db when entering view controller
    func getFoodFromDb() {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {return}
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Food")
        
        do {
            foods = try managedContext.fetch(fetchRequest) as? [Food] ?? []
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    
    // Creates a custom alert that allows user to edit food data
    func createAlert() {
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let alertVC = sb.instantiateViewController(identifier: "FoodAlertVC") as! FoodAlertViewController
        alertVC.parentVC = self
        alertVC.modalPresentationStyle = .overCurrentContext
        self.present(alertVC, animated: true, completion: nil)
    }
    
    // Back Button is pressed
    @IBAction func back(_ sender: Any) {
        self.dismiss(animated: true, completion: {})
    }
}
