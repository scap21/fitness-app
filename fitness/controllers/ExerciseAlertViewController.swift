//
//  ExerciseAlertViewController.swift
//  fitness
//
//  Created by Dahae Bae on 4/10/21.
//

import UIKit

class ExerciseAlertViewController: UIViewController {
    
    var parentVC: ExerciseDetailViewController!
    
    @IBOutlet var alertView: UIView!
    @IBOutlet var exerciseNameText: UITextField!
    @IBOutlet var calCountText: UITextField!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        editView()
        
        // Condition to keep name and calories for edits
        if parentVC.editItem == true {
            exerciseNameText.text = parentVC.exercises[parentVC.selectedIndex].name
            calCountText.text = parentVC.exercises[parentVC.selectedIndex].calories.description
        }
    }

    @IBAction func handleSave(_ sender: Any) {
        let tempCals = parentVC.exercises[parentVC.selectedIndex].calories
        parentVC.exercises[parentVC.selectedIndex].name = exerciseNameText.text ?? ""
        parentVC.exercises[parentVC.selectedIndex].calories = Double(calCountText.text ?? "0") ?? 0
        // update total calories count
        if tempCals > parentVC.exercises[parentVC.selectedIndex].calories {
            parentVC.totalCalories -= (tempCals - parentVC.exercises[parentVC.selectedIndex].calories)
        } else if tempCals < parentVC.exercises[parentVC.selectedIndex].calories {
            parentVC.totalCalories += (parentVC.exercises[parentVC.selectedIndex].calories - tempCals)
        }
        parentVC.caloriesCount.text = String(format: "%.2f", parentVC.totalCalories)
        parentVC.exerciseTableView.reloadData()
        self.dismiss(animated: true, completion: {})        }

    

    @IBAction func handleDelete(_ sender: Any) {
        // Remove calories from totalCalories
        parentVC.totalCalories = parentVC.totalCalories - parentVC.exercises[parentVC.selectedIndex].calories
        parentVC.caloriesCount.text = String(format: "%.2f", parentVC.totalCalories)
        // Delete exercise instance
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {return}
        let managedContext = appDelegate.persistentContainer.viewContext
        managedContext.delete(parentVC.exercises[parentVC.selectedIndex])
        do {
            try managedContext.save()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
        parentVC.exercises.remove(at: parentVC.selectedIndex)
        parentVC.exerciseTableView.reloadData()
        self.dismiss(animated: true, completion: {})        }
    
    // Customize the look of the alert view controller
    func editView() {
        alertView.layer.cornerRadius = 10.0
        alertView.layer.borderWidth = 2.5
        alertView.layer.borderColor = UIColor.systemBlue.cgColor
        exerciseNameText.becomeFirstResponder()
    }


}
