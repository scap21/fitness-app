//
//  MainViewController.swift
//  fitness
//
//  Created by Steven Capleton on 3/29/21.
//

import UIKit

class MainViewController: UIViewController {
    
    // Greetings Label
    @IBOutlet var greetingsLbl: UILabel!
    // Buttons
    @IBOutlet var profileButton: UIButton!
    @IBOutlet var exerciseButton: UIButton!
    @IBOutlet var foodButton: UIButton!
    
    // user
//    var user: User!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        greetingsLbl.text = "Hello \(UserDefaults.standard.string(forKey: "UserFirstname") ?? "")"
        // Do any additional setup after loading the view.
        
        editLook()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if !UserDefaults.standard.bool(forKey: "Registered") {
            let sb = UIStoryboard(name: "Main", bundle: nil)
            let registerVC = sb.instantiateViewController(withIdentifier: "EditProfileViewController") as! EditProfileViewController
            registerVC.parentVC = self
            registerVC.modalPresentationStyle = .overCurrentContext
            self.present(registerVC, animated: true, completion: self.viewDidLoad)
        }
    }
    
    // Profile Button Pressed
    @IBAction func handleProfileButton(_ sender: Any) {
    }
    
    // Exercise Button Pressed
    @IBAction func handleExerciseButton(_ sender: Any) {
    }
    
    // Food Button Pressed
    @IBAction func handleFoodButton(_ sender: Any) {
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "gotoProfileVC" {
            if let viewController = segue.destination as? ProfileDetailViewController {
                viewController.parentVC = self
            }
        }
    }
    
    // Edit the look of the ViewController
    func editLook() {
        profileButton.layer.cornerRadius = 20.0
        exerciseButton.layer.cornerRadius = 20.0
        foodButton.layer.cornerRadius = 20.0
    }

}
