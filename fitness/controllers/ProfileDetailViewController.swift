//
//  ProfileDetailViewController.swift
//  fitness
//
//  Created by Steven Capleton on 3/29/21.
//

import UIKit
//import CoreData
import HealthKit

class ProfileDetailViewController: UIViewController {
    /*
    Hey Zaid, here are the UserDefault strings for user data
     
    firstname:  "UserFirstname"
    lastname:   "UserLastname"
    age:        "UserAge"
    height:     "UserHeight"
    weight:     "UserWeight"
    gender:     "UserGender"
     
    */
    
    
    @IBOutlet weak var stepsLabel: UILabel!
    
    @IBOutlet var caloriesCount: UILabel!
    
    @IBOutlet var caloriesLabel: UILabel!
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var ageLabel: UILabel!
    
    @IBOutlet weak var weightLabel: UILabel!
    
    @IBOutlet weak var heightLabel: UILabel!
    
    @IBOutlet weak var genderLabel: UILabel!
    
    
    let healthStore = HKHealthStore()
    var parentVC: MainViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if HKHealthStore.isHealthDataAvailable() {
            
            let HKTypes: Set = [ HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.stepCount)! ]
            
            healthStore.requestAuthorization(toShare: HKTypes, read: HKTypes) { (success, error) in
                if (success) {
                    self.getSteps { (result) in
                        DispatchQueue.main.async {
                            let stepCount = String(Int(result))
                            self.stepsLabel.text = String(stepCount)
                        }
                    }
                }
            }
            
            let fname = UserDefaults.standard.string(forKey: "UserFirstname")
            let lname = UserDefaults.standard.string(forKey: "UserLastname")
            let age = UserDefaults.standard.string(forKey: "UserAge")
            let height = UserDefaults.standard.string(forKey: "UserHeight")
            let weight = UserDefaults.standard.string(forKey: "UserWeight")
            let gender = UserDefaults.standard.string(forKey: "UserGender")
            
            nameLabel.text = "\(fname ?? "") \(lname ?? "")"
//            lastNameLabel.text="Last Name: \(lname ?? "")"
            ageLabel.text = "\(age ?? "")"
            heightLabel.text = "\(height ?? "") \(UserDefaults.standard.string(forKey: "HeightPref") ?? "")"
            weightLabel.text = "\(weight ?? "") \(UserDefaults.standard.string(forKey: "WeightPref") ?? "")"
            genderLabel.text = "\(gender ?? "")"

            // Count Net Calories
            let calsBurned = UserDefaults.standard.double(forKey: "CaloriesBurned")
            let calsGained = UserDefaults.standard.double(forKey: "CaloriesGained")
            if calsBurned > calsGained {
                caloriesLabel.text = "Calories Burned"
                caloriesCount.text = String(calsBurned - calsGained)
            } else if calsGained > calsBurned {
                caloriesLabel.text = "Calories Gained"
                caloriesCount.text = String(calsGained - calsBurned)
            } else {
                caloriesCount.text = "0"
            }
        
        }
        
        // Do any additional setup after loading the view.
    }
    
    func getSteps(completion: @escaping (Double) -> Void) {
        let type = HKQuantityType.quantityType(forIdentifier: .stepCount)!
        let now = Date()
        let start = Calendar.current.startOfDay(for: now)
        var interval = DateComponents()
        interval.day = 1
        let query = HKStatisticsCollectionQuery(quantityType: type, quantitySamplePredicate: nil, options: [.cumulativeSum], anchorDate: start, intervalComponents: interval)
        
        query.initialResultsHandler = { _, result, err in
                var resultCount = 0.0
                result!.enumerateStatistics(from: start, to: now) { statistics, _ in

                if let sum = statistics.sumQuantity() {
                    
                    resultCount = sum.doubleValue(for: HKUnit.count())
                }

            
                DispatchQueue.main.async {
                    completion(resultCount)
                }
            }
        }
        
        query.statisticsUpdateHandler = {
            query, statistics, statisticsCollection, error in

            // If new statistics are available
            if let sum = statistics?.sumQuantity() {
                let resultCount = sum.doubleValue(for: HKUnit.count())
                // Return
                DispatchQueue.main.async {
                    completion(resultCount)
                }
            }
        }

        healthStore.execute(query)

        
    }
    
    
    // Navigation Bar Buttons
    // Edit button clicked go to EditProfileViewController
    @IBAction func handleEditProfile(_ sender: Any) {
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let editVC = sb.instantiateViewController(withIdentifier: "EditProfileViewController") as! EditProfileViewController
        editVC.parentVC = self
        editVC.modalPresentationStyle = .overCurrentContext
        self.present(editVC, animated: true, completion: nil)
    }
    
    // Back button is clicked
    @IBAction func back(_ sender: Any) {
        self.parentVC.viewDidLoad() // Updates MainViewController (if edit profile name then reflects change in greeting)
        self.dismiss(animated: true, completion: {})
    }
    
}
