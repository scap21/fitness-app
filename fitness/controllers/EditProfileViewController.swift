//
//  EditProfileViewController.swift
//  fitness
//
//  Created by Steven Capleton on 4/12/21.
//

import UIKit

class EditProfileViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    //TextFields
    @IBOutlet var firstname: UITextField!
    @IBOutlet var lastname: UITextField!
    @IBOutlet var age: UITextField!
    @IBOutlet var height: UITextField!
    @IBOutlet var weight: UITextField!
    @IBOutlet var gender: UIPickerView!
    
    //Height Button Group
    @IBOutlet var inches: UIButton!
    @IBOutlet var meters: UIButton!
    @IBOutlet var feet: UIButton!
    
    //Weight Button Group
    @IBOutlet var lbs: UIButton!
    @IBOutlet var kg: UIButton!
    
    //Error Marks
    @IBOutlet var errorStatement: UILabel!
    @IBOutlet var errorFirstname: UILabel!
    @IBOutlet var errorLastname: UILabel!
    @IBOutlet var errorAge: UILabel!
    @IBOutlet var errorHeight: UILabel!
    @IBOutlet var errorWeight: UILabel!
    
    let genders = ["Male", "Female"]
    var parentVC: UIViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Load Data into TextFields
        firstname.text = UserDefaults.standard.string(forKey: "UserFirstname") ?? ""
        lastname.text = UserDefaults.standard.string(forKey: "UserLastname") ?? ""
        age.text = UserDefaults.standard.string(forKey: "UserAge") ?? ""
        height.text = UserDefaults.standard.string(forKey: "UserHeight") ?? ""
        weight.text = UserDefaults.standard.string(forKey: "UserWeight") ?? ""
        
        gender.delegate = self
        gender.dataSource = self
        gender.selectRow(UserDefaults.standard.integer(forKey: "GenderPref"), inComponent: 0, animated: false)
        
        // Button Groups Defaults
        if (UserDefaults.standard.string(forKey: "HeightPref") == nil) || UserDefaults.standard.string(forKey: "HeightPref") == "Inches" {
            inches.isSelected = true
            UserDefaults.standard.set("Inches",forKey: "HeightPref")
        } else if UserDefaults.standard.string(forKey: "HeightPref") == "Meters" {
            meters.isSelected = true
        } else {
            feet.isSelected = true
        }
        
        if UserDefaults.standard.string(forKey: "WeightPref") == nil || UserDefaults.standard.string(forKey: "WeightPref") == "Pounds" {
            lbs.isSelected = true
            UserDefaults.standard.set("Pounds",forKey: "WeightPref")
        } else {
            kg.isSelected = true
        }
        
        if UserDefaults.standard.string(forKey: "GenderPref") == nil {
            UserDefaults.standard.set("Male",forKey: "UserGender")
        }
    }
    
    // Populate PickerView with gender info
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return genders.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return genders[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        UserDefaults.standard.set(row, forKey: "GenderPref")
        UserDefaults.standard.set(genders[row], forKey: "UserGender")
    }
    
    // Handle Height Button Group
    @IBAction func heightInchesButton(_ sender: Any) {
        inches.isSelected = true
        meters.isSelected = false
        feet.isSelected = false
        
        if UserDefaults.standard.string(forKey: "HeightPref") == "Meters" && height.text != "" {
            let tmp = Double(height.text ?? "0")
            height.text = String(format: "%.2f", tmp!*39.36)
        } else if UserDefaults.standard.string(forKey: "HeightPref") == "Feet" && height.text != "" {
            let tmp = Double(height.text ?? "0")
            height.text = String(format: "%.2f", tmp!*12)
        }
        
        UserDefaults.standard.setValue("Inches", forKey: "HeightPref")
    }
    @IBAction func heightMetersButton(_ sender: Any) {
        meters.isSelected = true
        inches.isSelected = false
        feet.isSelected = false
        
        if UserDefaults.standard.string(forKey: "HeightPref") == "Inches" && height.text != "" {
            let tmp = Double(height.text ?? "0")
            height.text = String(format: "%.2f", tmp!/39.36)
        } else if UserDefaults.standard.string(forKey: "HeightPref") == "Feet" && height.text != "" {
            let tmp = Double(height.text ?? "0")
            height.text = String(format: "%.2f", tmp!/3.28)
        }
        
        UserDefaults.standard.set("Meters", forKey: "HeightPref")
    }
    @IBAction func heightFtButton(_ sender: Any) {
        feet.isSelected = true
        inches.isSelected = false
        meters.isSelected = false
        
        if UserDefaults.standard.string(forKey: "HeightPref") == "Inches" && height.text != "" {
            let tmp = Double(height.text ?? "0")
            height.text = String(format: "%.2f", tmp!/12)
        } else if UserDefaults.standard.string(forKey: "HeightPref") == "Meters" && height.text != "" {
            let tmp = Double(height.text ?? "0")
            height.text = String(format: "%.2f", tmp!*3.28)
        }
        
        UserDefaults.standard.set("Feet", forKey: "HeightPref")
    }
    
    // Handle Weight Button Group
    @IBAction func weightPoundsButton(_ sender: Any) {
        lbs.isSelected = true
        kg.isSelected = false
        UserDefaults.standard.set("Pounds", forKey: "WeightPref")
        
        if weight.text != "" {
            let tmp = Double(weight.text ?? "0")
            weight.text = String(format: "%.2f", tmp!*2.2046226218)
        }
    }
    @IBAction func weightKgButton(_ sender: Any) {
        kg.isSelected = true
        lbs.isSelected = false
        UserDefaults.standard.set("Kilograms", forKey: "WeightPref")
        
        if weight.text != "" {
            let tmp = Double(weight.text ?? "0")
            weight.text = String(format: "%.2f", tmp!/2.2046226218)
        }
    }
    
    // Accept Changes and Exit
    @IBAction func done(_ sender: Any) {
        //Pref Errors
        if (firstname.text != "" && lastname.text != "" && age.text != "" && height.text != "" && weight.text != "") {
            //if error is showing clear
            errorStatement.isHidden = true
            
            //store values in UserDefaults
            UserDefaults.standard.set(firstname.text, forKey: "UserFirstname")
            UserDefaults.standard.set(lastname.text, forKey: "UserLastname")
            UserDefaults.standard.set(age.text, forKey: "UserAge")
            //Store doubles of weight and height
            let heightDouble = Double(height.text!)
            let weightDouble = Double(weight.text!)
            UserDefaults.standard.set(String(heightDouble!), forKey: "UserHeight")
            UserDefaults.standard.set(String(weightDouble!), forKey: "UserWeight")
            UserDefaults.standard.set(true, forKey: "Registered")
            self.parentVC.viewDidLoad() //Reload ParentViewController
            self.dismiss(animated: true, completion: {})
        }
        
        //Firstname Error
        if firstname.text == "" {
            errorStatement.isHidden = false
            errorFirstname.isHidden = false
        } else { errorFirstname.isHidden = true }
        //Lastname Error
        if lastname.text == "" {
            errorStatement.isHidden = false
            errorLastname.isHidden = false
        } else { errorLastname.isHidden = true }
        //Age Error
        if age.text == "" {
            errorStatement.isHidden = false
            errorAge.isHidden = false
        } else { errorAge.isHidden = true }
        //Height Error
        if height.text == "" {
            errorStatement.isHidden = false
            errorHeight.isHidden = false
        } else { errorHeight.isHidden = true }
        //Weight Error
        if weight.text == "" {
            errorStatement.isHidden = false
            errorWeight.isHidden = false
        } else { errorWeight.isHidden = true }
    }
}
