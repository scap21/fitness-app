//
//  FoodAlertViewController.swift
//  fitness
//
//  Created by Steven Capleton on 3/29/21.
//

import UIKit

class FoodAlertViewController: UIViewController {
    
    var parentVC: FoodDetailViewController!
    @IBOutlet var alertView: UIView!
    @IBOutlet var foodNameText: UITextField!
    @IBOutlet var calCountText: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        editView()
        
        // Condition to keep name and calories for edits
        if parentVC.editItem == true {
            foodNameText.text = parentVC.foods[parentVC.selectedIndex].name
            calCountText.text = parentVC.foods[parentVC.selectedIndex].calories.description
        }
    }
    
    // Save the data in text fields
    @IBAction func handleSave(_ sender: Any) {
        let tempCals = parentVC.foods[parentVC.selectedIndex].calories
        parentVC.foods[parentVC.selectedIndex].name = foodNameText.text ?? ""
        parentVC.foods[parentVC.selectedIndex].calories = Double(calCountText.text ?? "0") ?? 0
        // update total calories count
        if tempCals > parentVC.foods[parentVC.selectedIndex].calories {
            parentVC.totalCalories -= (tempCals - parentVC.foods[parentVC.selectedIndex].calories)
        } else if tempCals < parentVC.foods[parentVC.selectedIndex].calories {
            parentVC.totalCalories += (parentVC.foods[parentVC.selectedIndex].calories - tempCals)
        }
        parentVC.caloriesCount.text = String(format: "%.2f", parentVC.totalCalories)
        parentVC.foodTableView.reloadData()
        self.dismiss(animated: true, completion: {})
    }
    
    // Delete the instance of food in core data
    @IBAction func handleDelete(_ sender: Any) {
        // Remove calories from totalCalories
        parentVC.totalCalories = parentVC.totalCalories - parentVC.foods[parentVC.selectedIndex].calories
        parentVC.caloriesCount.text = String(format: "%.2f", parentVC.totalCalories)
        // Delete food instance
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {return}
        let managedContext = appDelegate.persistentContainer.viewContext
        managedContext.delete(parentVC.foods[parentVC.selectedIndex])
        do {
            try managedContext.save()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
        parentVC.foods.remove(at: parentVC.selectedIndex)
        parentVC.foodTableView.reloadData()
        self.dismiss(animated: true, completion: {})
    }
    
    // Customize the look of the alert view controller
    func editView() {
        alertView.layer.cornerRadius = 10.0
        alertView.layer.borderWidth = 2.5
        alertView.layer.borderColor = UIColor.systemBlue.cgColor
        foodNameText.becomeFirstResponder()
    }
}
