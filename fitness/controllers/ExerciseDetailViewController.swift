//
//  ExerciseDetailViewController.swift
//  fitness
//
//  Created by Steven Capleton on 3/29/21.
//

import UIKit
import CoreData

class ExerciseDetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    //Outlet Identifiers
    @IBOutlet var exerciseTableView: UITableView!
    @IBOutlet var caloriesCount: UILabel!
    
    //Other Variables
    var parentVC: MainViewController!
    var editItem: Bool!
    var exercises = [Exercise]()
    var selectedIndex: Int!
    var totalCalories: Double!
    
    //When view is loaded do initial setup
    override func viewDidLoad() {
        super.viewDidLoad()

        totalCalories = UserDefaults.standard.double(forKey: "CaloriesBurned")
        caloriesCount.text = String(format: "%.2f", totalCalories)
        
        exerciseTableView.delegate = self
        exerciseTableView.dataSource = self
        
        //If returning to view controller retrieve exercises from db
        getExerciseFromDb()
    }
    
    //When view is closed save total calories to user defaults to load later
    override func viewDidDisappear(_ animated: Bool) {
        if exercises.count == 0 { UserDefaults.standard.set(0.0, forKey: "CaloriesBurned") }
        else { UserDefaults.standard.set(totalCalories, forKey: "CaloriesBurned") }
    }
    
    // Add Button Pressed
    @IBAction func handleAddExercise(_ sender: Any) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {return}
        let managedContext = appDelegate.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "Exercise", in: managedContext)!
        
        let exerciseItem = NSManagedObject(entity: entity, insertInto: managedContext) as! Exercise
        
        do {
            selectedIndex = exercises.count
            editItem = false
            createAlert()
            exercises.append(exerciseItem)
            try managedContext.save()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    //Get the table item count
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return exercises.count
    }
    
    //Populate table data
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ExerciseCell", for: indexPath) as!
            ExerciseTableCell
        cell.selectionStyle = .none
        cell.exerciseName.text = exercises[indexPath.row].name
        cell.calCount.text = exercises[indexPath.row].calories.description
        return cell
    }
    
    //Perform action at selected cell
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        editItem = true
        createAlert()
    }
    
    //Retrieve exercise items frm db when entering view controller
    func getExerciseFromDb() {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {return}
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Exercise")
        
        do {
            exercises = try managedContext.fetch(fetchRequest) as? [Exercise] ?? []
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    
    //Creates a custom alert that allows user to edit exericse data
    func createAlert() {
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let alertVC = sb.instantiateViewController(identifier: "ExerciseAlertVC") as! ExerciseAlertViewController
        alertVC.parentVC = self
        alertVC.modalPresentationStyle = .overCurrentContext
        self.present(alertVC, animated: true, completion: nil)
    }

    @IBAction func back(_ sender: Any) {
        self.dismiss(animated: true, completion: {})
    }
}
