//
//  FoodTableCell.swift
//  fitness
//
//  Created by Steven Capleton on 3/29/21.
//

import UIKit

class FoodTableCell: UITableViewCell {
    @IBOutlet var foodName: UILabel!
    @IBOutlet var calCount: UILabel!
}
