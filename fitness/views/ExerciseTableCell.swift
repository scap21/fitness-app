//
//  ExerciseTableCell.swift
//  fitness
//
//  Created by Dahae Bae on 4/10/21.
//

import UIKit

class ExerciseTableCell: UITableViewCell {

    @IBOutlet var exerciseName: UILabel!
    @IBOutlet var calCount: UILabel!
}
